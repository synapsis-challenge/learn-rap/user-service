package helper

import (
	"strconv"
	"time"

	"learn-raa/user-service/config"

	"github.com/golang-jwt/jwt"
	_ "github.com/joho/godotenv/autoload"
)

type PermissionRole struct {
	PermissionId   interface{} `json:"id"`
	PermissionName interface{} `json:"name"`
}

type Claims struct {
	Level      string           `json:"level"`
	Permission []PermissionRole `json:"permissions"`
	jwt.StandardClaims
}

func GenerateJwt(issuer, level string, permission []PermissionRole) (string, error) {
	session, _ := strconv.Atoi(config.SessionLogin)
	claims := &Claims{
		Level:      level,
		Permission: permission,
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(session)).Unix(),
		},
	}
	tokens := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return tokens.SignedString([]byte(config.SecretKey))
}

func ParseJwt(cookie string) (string, string, []PermissionRole, error) {
	var claims Claims
	token, err := jwt.ParseWithClaims(cookie, &claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretKey), nil
	})
	if err != nil || !token.Valid {
		return "", "", []PermissionRole{}, err
	}

	return claims.Issuer, claims.Level, claims.Permission, nil
}

// refresh
func GenerateRefreshJwt(issuer string) (string, error) {
	session, _ := strconv.Atoi(config.SessionRefreshToken)
	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(session)).Unix(),
		},
	}
	tokens := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return tokens.SignedString([]byte(config.SecretKeyRefresh))
}

func ParseRefreshJwt(cookie string) (string, error) {
	var claims jwt.StandardClaims
	token, err := jwt.ParseWithClaims(cookie, &claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretKeyRefresh), nil
	})
	if err != nil || !token.Valid {
		return "", err
	}

	return claims.Issuer, err
}
