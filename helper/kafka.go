package helper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"learn-raa/user-service/config"
	"learn-raa/user-service/exception"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	_ "github.com/joho/godotenv/autoload"
)

func ProduceToKafka(data interface{}, action, kafkaTopic string) {
	broker := fmt.Sprintf("%s:%s", config.KafkaHost, config.KafkaPort)

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": broker})

	if err != nil {
		log.Printf("KAFKA | Failed to create producer: %s\n", err)
	}

	value := new(bytes.Buffer)
	json.NewEncoder(value).Encode(data)

	p.ProduceChannel() <- &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &kafkaTopic, Partition: kafka.PartitionAny},
		Value:          value.Bytes(),
		Headers:        []kafka.Header{{Key: "method", Value: []byte(action)}},
	}

	for e := range p.Events() {
		switch ev := e.(type) {
		case *kafka.Message:
			m := ev
			if m.TopicPartition.Error != nil {
				log.Printf("KAFKA | Delivery failed: %v\n", m.TopicPartition.Error)
				p.Close()
			} else {
				log.Printf("KAFKA | Delivered message to topic %s [%d] at offset %v\n",
					*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
				p.Close()
			}
			return

		default:
			if strings.Contains(ev.String(), "failed:") {
				p.Close()
				panic(exception.ErrorInternalServer("message broker error"))
			} else {
				log.Printf("KAFKA | Ignored event: %s\n", ev)
			}
		}
	}

	p.Close()
}
