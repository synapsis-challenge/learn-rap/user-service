package repository

import (
	"context"

	"learn-raa/user-service/model/domain"

	"github.com/jackc/pgx/v5"
)

type UserRepositoryImpl struct {
	DB Store
}

func NewUserRepository(db Store) UserRepository {
	return &UserRepositoryImpl{
		DB: db,
	}
}

func (repository *UserRepositoryImpl) CreateUserTx(ctx context.Context, user domain.User) error {

	err := repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err := repository.CreateUser(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (repository *UserRepositoryImpl) FindUserNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindUserNotDeletedByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *UserRepositoryImpl) FindUserDomainNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.UserDomain, error) {

	var data domain.UserDomain
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindUserDomainNotDeletedByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *UserRepositoryImpl) FindUserByQueryTx(ctx context.Context, query, value string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindUserByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *UserRepositoryImpl) FindAllNotDeletedUserTx(ctx context.Context, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) ([]domain.UserDomain, error) {

	var data []domain.UserDomain
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindAllNotDeletedUser(ctx, tx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *UserRepositoryImpl) CountAllNotDeletedUserTx(ctx context.Context, search, departement, project, positon, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) (interface{}, error) {

	var data interface{}
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.CountAllNotDeletedUser(ctx, tx, search, departement, project, positon, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *UserRepositoryImpl) UpdatePasswordTx(ctx context.Context, user domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.UpdatePasswordUser(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (repository *UserRepositoryImpl) UpdateUserTx(ctx context.Context, user domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.UpdateUser(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (repository *UserRepositoryImpl) DeleteUserTx(ctx context.Context, user domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.DeleteUser(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}
