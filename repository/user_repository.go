package repository

import (
	"context"

	"learn-raa/user-service/model/domain"
)

type UserRepository interface {
	CreateUserTx(ctx context.Context, user domain.User) error
	FindUserNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error)
	FindUserDomainNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.UserDomain, error) // this is the relevant query to users table's field data type
	FindUserByQueryTx(ctx context.Context, query, value string) (domain.User, error)
	FindAllNotDeletedUserTx(ctx context.Context, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) ([]domain.UserDomain, error)
	CountAllNotDeletedUserTx(ctx context.Context, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) (interface{}, error)
	UpdatePasswordTx(ctx context.Context, user domain.User) error
	UpdateUserTx(ctx context.Context, user domain.User) error
	DeleteUserTx(ctx context.Context, user domain.User) error
}
