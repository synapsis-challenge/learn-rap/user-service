package repository

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"learn-raa/user-service/config"
	"learn-raa/user-service/model/domain"

	"github.com/jackc/pgx/v5"
)

func (repository *UserRepositoryImpl) CreateUser(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := fmt.Sprintf(`INSERT INTO %s (nik, name, email, password, phone, status, created_at, updated_at, nomor_wa) 
	VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9)`, "users")
	_, err := db.Prepare(context.Background(), "create_user", query)
	if err != nil {
		log.Println(err)
		return err
	}

	_, err = db.Exec(context.Background(), "create_user",
		user.NIK, user.Name, user.Email, user.Password, user.Phone, user.Status, user.CreatedAt, user.UpdatedAt, user.NomorWa)

	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (repository *UserRepositoryImpl) FindUserNotDeletedByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.User, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	WHERE %s = $1 AND deleted_at is NULL`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *UserRepositoryImpl) FindUserDomainNotDeletedByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.UserDomain, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE %s = $1 AND deleted_at is NULL`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.UserDomain{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.UserDomain])

	if err != nil {
		return domain.UserDomain{}, err
	}

	return data, nil
}

func (repository *UserRepositoryImpl) FindUserByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.User, error) {
	queryStr := fmt.Sprintf(`SELECT u.*, coalesce(r.name,''), coalesce(d.name,''), coalesce(p.name,''), coalesce(pst.name,''), coalesce(g.name,''), coalesce(rlg.name,'') FROM %s u
	LEFT JOIN roles r ON u.role_id = r.id
	LEFT JOIN departements d ON u.departement_id = d.id
	LEFT JOIN projects p ON u.project_id = p.id 
	LEFT JOIN positions pst ON pst.id = u.position_id
	LEFT JOIN genders g ON g.id = u.gender
	LEFT JOIN religions rlg ON rlg.id = u.religion
	WHERE %s = $1`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *UserRepositoryImpl) FindAllNotDeletedUser(ctx context.Context, db pgx.Tx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) ([]domain.UserDomain, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE deleted_at is NULL`, "users")

	queryStr += ` ORDER BY u.name ASC`

	queryStr = QueryPage(page, limit, queryStr)

	user, err := db.Query(context.Background(), queryStr)

	if err != nil {
		log.Println(err)
		return []domain.UserDomain{}, err
	}

	defer user.Close()

	data, err := pgx.CollectRows(user, pgx.RowToStructByPos[domain.UserDomain])

	if err != nil {
		log.Println(data, err)
		return []domain.UserDomain{}, err
	}

	return data, nil
}

func (repository *UserRepositoryImpl) CountAllNotDeletedUser(ctx context.Context, db pgx.Tx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject string, searchNik string) (interface{}, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE deleted_at is NULL`, "users")

	if departement != "" {
		departementid, _ := strconv.Atoi(departement)
		queryStr += fmt.Sprintf(" AND u.departement_id = %d", departementid)
	}

	if project != "" {
		projectid, _ := strconv.Atoi(project)
		queryStr += fmt.Sprintf(" AND u.project_id = %d", projectid)
	}

	if position != "" {
		positionid, _ := strconv.Atoi(position)
		queryStr += fmt.Sprintf(" AND u.position_id = %d", positionid)
	}

	if searchName != "" {
		queryStr += fmt.Sprintf(" AND LOWER(u.name) LIKE LOWER('%%%s%%')", searchName)
	}

	if searchRole != "" {
		queryStr += fmt.Sprintf(" AND LOWER(r.name) LIKE LOWER('%%%s%%')", searchRole)
	}

	if searchDepartement != "" {
		queryStr += fmt.Sprintf(" AND LOWER(d.name) LIKE LOWER('%%%s%%')", searchDepartement)
	}

	if searchProject != "" {
		queryStr += fmt.Sprintf(" AND LOWER(p.name) LIKE LOWER('%%%s%%')", searchProject)
	}

	if searchNik != "" {
		queryStr += fmt.Sprintf(" AND LOWER(u.nik) in (%s)", searchNik)
	}

	if searchPosition != "" {
		queryStr += fmt.Sprintf(" AND LOWER(pst.name) LIKE LOWER('%%%s%%')", searchPosition)
	}

	if search != "" {
		queryStr += fmt.Sprintf(" AND (LOWER(u.name) LIKE LOWER('%%%s%%') OR LOWER(u.nik) LIKE LOWER('%%%s%%'))", search, search)
	}

	user, err := db.Query(context.Background(), queryStr)

	if err != nil {
		log.Println(err)
		return []domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectRows(user, pgx.RowToStructByPos[domain.UserDomain])

	if err != nil {
		return []domain.User{}, err
	}

	return len(data), nil
}

func (repository *UserRepositoryImpl) UpdateUser(ctx context.Context, db pgx.Tx, user domain.User) error {
	// query := fmt.Sprintf("UPDATE %s SET name = $1, email = $2, phone = $3, updated_at = $4, role_id = $5, departement_id = $6, project_id = $7, position_id = $8, nomor_wa = $9 WHERE nik = $10 AND deleted_at is NULL", "users")
	query := fmt.Sprintf("UPDATE %s SET name = $1, email = $2, phone = $3, updated_at = $4, nomor_wa = $5 WHERE nik = $6 AND deleted_at is NULL", "users")
	_, err := db.Prepare(context.Background(), "update_user", query)
	if err != nil {
		return err
	}

	// data, err := db.Query(context.Background(), "update_user",
	// 	user.Name, user.Email, user.Phone, user.UpdatedAt, user.RoleId, user.DepartementId, user.ProjectId, user.PositionId, user.NomorWa, user.NIK)
	data, err := db.Query(context.Background(), "update_user",
		user.Name, user.Email, user.Phone, user.UpdatedAt, user.NomorWa, user.NIK)

	if err != nil {
		return err
	}

	defer data.Close()

	return nil
}

func (repository *UserRepositoryImpl) UpdatePasswordUser(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := "UPDATE users SET password = $1 WHERE nik = $2"
	_, err := db.Prepare(context.Background(), "update_pass", query)
	if err != nil {
		return err
	}

	data, err := db.Query(context.Background(), "update_pass", user.Password, user.NIK)
	if err != nil {
		return err
	}

	defer data.Close()
	return nil
}

func (repository *UserRepositoryImpl) DeleteUser(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := "UPDATE users SET deleted_at = $1 WHERE nik = $2"
	_, err := db.Prepare(context.Background(), "delete_user", query)
	if err != nil {
		return err
	}

	data, err := db.Query(context.Background(), "delete_user", user.DeletedAt, user.NIK)
	if err != nil {
		return err
	}

	defer data.Close()
	return nil
}

// Query page limit
func QueryPage(page, limit, queryStr string) string {
	defaultLimit, _ := strconv.Atoi(config.DefaultLimit)
	if page != "" {
		pageInt, _ := strconv.Atoi(page)
		if limit != "" {
			limitInt, _ := strconv.Atoi(limit)
			queryStr += fmt.Sprintf(" OFFSET %d LIMIT %d", (pageInt-1)*limitInt, limitInt)
		} else {
			queryStr += fmt.Sprintf(" OFFSET  %d LIMIT %d", (pageInt-1)*defaultLimit, defaultLimit)
		}
	} else {
		if limit != "" {
			limitInt, _ := strconv.Atoi(limit)
			queryStr += fmt.Sprintf(" LIMIT %d", limitInt)
		}
	}

	return queryStr
}
