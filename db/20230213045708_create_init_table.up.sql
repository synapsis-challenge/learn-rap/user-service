CREATE TABLE users (
    "id" SERIAL PRIMARY KEY,
    "nik" VARCHAR(50) NOT NULL UNIQUE,
    "name" VARCHAR(50) NOT NULL,
    "email" VARCHAR(50) NOT NULL UNIQUE,
    "password" VARCHAR(255) NOT NULL,
    "phone" VARCHAR(15),
    "status" INT NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    "nomor_wa" VARCHAR(15)
);
