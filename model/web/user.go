package web

import "time"

// Request
type UserCreateRequest struct {
	LastNik  string `json:"last_nik"`
	NIK      string `json:"nik" validate:"required"`
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	Phone    string `json:"phone" validate:"required"`
	NomorWa  string `json:"nomor_wa"`
	Status   *int   `json:"status" validate:"required"`
}

type UserUpdateRequest struct {
	NIK     string `json:"nik" validate:"required"`
	Name    string `json:"name" validate:"required"`
	Email   string `json:"email" validate:"required"`
	Phone   string `json:"phone" validate:"required"`
	NomorWa string `json:"nomor_wa"`
}

type BulkUserUpdateStatusRequest struct {
	Status  *int   `json:"status"`
	NikUser string `json:"user"`
	Reason  string `json:"reason"`
}

type UserUpdateStatusRequest struct {
	NIK    string `json:"nik" validate:"required"`
	Reason string `json:"reason"`
}

type UserUpdatePasswordRequest struct {
	NIK      string `json:"nik" validate:"required"`
	Password string `json:"password" validate:"required"`
}

// Response
type UserResponse struct {
	Id        *int       `json:"id"`
	NIK       string     `json:"nik"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Password  string     `json:"password"`
	Phone     string     `json:"phone"`
	NomorWa   string     `json:"nomor_wa"`
	Status    *int       `json:"status"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
}
