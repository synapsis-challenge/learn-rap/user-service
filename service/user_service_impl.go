package service

import (
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	"learn-raa/user-service/config"
	"learn-raa/user-service/exception"
	"learn-raa/user-service/helper"
	"learn-raa/user-service/model/domain"
	"learn-raa/user-service/model/web"
	"learn-raa/user-service/repository"

	"github.com/go-playground/validator"
	"github.com/gofiber/fiber/v2"
)

type UserServiceImpl struct {
	UserRepository repository.UserRepository
	Validate       *validator.Validate
}

type UserService interface {
	CreateUser(ctx *fiber.Ctx, request web.UserCreateRequest) (web.UserResponse, error)
	FindUserNotDeleteByNik(ctx *fiber.Ctx, nik string) (web.UserResponse, error)
	FindAllNotDeletedUser(ctx *fiber.Ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) ([]web.UserResponse, error)
	CountAllNotDeletedUser(ctx *fiber.Ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) (interface{}, error)
	UpdateUser(ctx *fiber.Ctx, request web.UserUpdateRequest) (web.UserResponse, error)
	UpdatePasswordUser(ctx *fiber.Ctx, request web.UserUpdatePasswordRequest) (web.UserResponse, error)
	DeleteUser(ctx *fiber.Ctx, nik string) error
}

func NewUserService(userRepository repository.UserRepository, validate *validator.Validate) UserService {
	return &UserServiceImpl{
		UserRepository: userRepository,
		Validate:       validate,
	}
}

func (service *UserServiceImpl) CreateUser(ctx *fiber.Ctx, request web.UserCreateRequest) (web.UserResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return web.UserResponse{}, err
	}

	regex := regexp.MustCompile("^[A-Za-z0-9]+$") // only letter and number

	if !regex.MatchString(request.NIK) {
		return web.UserResponse{}, exception.ErrorBadRequest("NIK only contains letters and number")
	}

	user, _ := service.UserRepository.FindUserByQueryTx(ctx.Context(), "nik", request.NIK)
	if user.NIK != "" {
		return web.UserResponse{}, exception.ErrorBadRequest("NIK already exist.")
	}

	if len(request.Name) > 50 {
		return web.UserResponse{}, exception.ErrorBadRequest("Name should les then equal 50 character.")
	}
	if len(request.NIK) > 7 {
		return web.UserResponse{}, exception.ErrorBadRequest("NIK should les then equal 7 character.")
	}

	if !helper.IsEmailValid(request.Email) {
		return web.UserResponse{}, exception.ErrorBadRequest("Not valid email.")
	}

	if len(request.Password) < 6 {
		return web.UserResponse{}, exception.ErrorBadRequest("Password lenght should more then equal 6 character.")
	}

	createdUser := domain.User{
		NIK:       request.NIK,
		Name:      request.Name,
		Email:     request.Email,
		Phone:     request.Phone,
		NomorWa:   request.NomorWa,
		Status:    request.Status,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	createdUser.SetPassword(request.Password)

	var createErr error

	if request.Phone != "" {
		userPhone, _ := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "phone", request.Phone)
		if userPhone.NIK != "" && userPhone.NIK != request.NIK {
			err = exception.ErrorBadRequest("Phone exist.")
			createErr = err

		}
	}

	if request.NomorWa != "" {
		userPhone, _ := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "nomor_wa", request.NomorWa)
		if userPhone.NIK != "" && userPhone.NIK != request.NIK {
			err = exception.ErrorBadRequest("Nomor WA exist.")
			createErr = err

		}
	}

	err = service.UserRepository.CreateUserTx(ctx.Context(), createdUser)

	if err != nil {
		if strings.Contains(err.Error(), "unique") && strings.Contains(err.Error(), "users_nik_key") {
			err = exception.ErrorBadRequest("NIK already exist.")
			createErr = err

		}
		if strings.Contains(err.Error(), "unique") && strings.Contains(err.Error(), "users_email_key") {
			err = exception.ErrorBadRequest("Email already exist.")
			createErr = err
		}
	}

	//rollback rehired status to previous status(inactive) when error at creating
	if createErr != nil {

		// errRollback := service.UserRepository.UpdateStatusTx(ctx.Context(), lastNikUser)
		// if errRollback != nil {
		// 	return web.UserResponse{}, errRollback
		// }

		// errRollback = service.UserRepository.UpdateUserTx(ctx.Context(), lastNikUser)
		// if errRollback != nil {
		// 	return web.UserResponse{}, errRollback
		// }

		// return web.UserResponse{}, createErr
	}

	if request.LastNik != "" {

		user, _ = service.UserRepository.FindUserNotDeleteByQueryTx(ctx.Context(), "nik", createdUser.NIK)

		rehired := domain.RehiredUser{Nik: user.NIK, LastNik: request.LastNik}

		helper.ProduceToKafka(rehired, "POST.REHIRED", config.KafkaTopic)

	}

	user, err = service.UserRepository.FindUserNotDeleteByQueryTx(ctx.Context(), "nik", createdUser.NIK)
	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not exist")
	}

	// kafka prooduce
	helper.ProduceToKafka(user, "POST.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "POST.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "POST.USER", config.KafkaTopicProfile)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisSet(redisKey, user)
	if err != nil {
		log.Println(err)
	}

	// redis get
	redisData, err := helper.RedisGet(redisKey)
	if err != nil {
		log.Println(err)
	}
	log.Println("REDIS | " + redisData)

	return domain.ToUserResponse(user), nil
}

func (service *UserServiceImpl) FindUserNotDeleteByNik(ctx *fiber.Ctx, nik string) (web.UserResponse, error) {
	user, err := service.UserRepository.FindUserNotDeleteByQueryTx(ctx.Context(), "nik", nik)

	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not found.")
	}

	return domain.ToUserResponse(user), nil
}

func (service *UserServiceImpl) FindAllNotDeletedUser(ctx *fiber.Ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) ([]web.UserResponse, error) {
	searchNiks := ""
	if searchNik != "" {
		niks := strings.Split(searchNik, ",")
		for _, nik := range niks {
			searchNiks += fmt.Sprintf("'%s',", strings.ToLower(nik))
		}

		// trim the last ,
		searchNiks = searchNiks[0 : len(searchNiks)-1]
	}

	users, err := service.UserRepository.FindAllNotDeletedUserTx(ctx.Context(), search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNiks)
	if err != nil {
		log.Println("service", users, err.Error())
	}
	return domain.ToAllUserDomainResponses(users), nil
}

func (service *UserServiceImpl) CountAllNotDeletedUser(ctx *fiber.Ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik string) (interface{}, error) {
	searchNiks := ""
	if searchNik != "" {
		niks := strings.Split(searchNik, ",")
		for _, nik := range niks {
			searchNiks += fmt.Sprintf("'%s',", nik)
		}

		// trim the last ,
		searchNiks = searchNiks[0 : len(searchNiks)-1]
	}

	users, _ := service.UserRepository.CountAllNotDeletedUserTx(ctx.Context(), search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNiks)
	return users, nil
}

func (service *UserServiceImpl) UpdateUser(ctx *fiber.Ctx, request web.UserUpdateRequest) (web.UserResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return web.UserResponse{}, err
	}

	if len(request.Name) > 50 {
		return web.UserResponse{}, exception.ErrorBadRequest("Name length should less then equal 50 character.")
	}

	if !helper.IsEmailValid(request.Email) {
		return web.UserResponse{}, exception.ErrorBadRequest("Not valid email.")
	}

	user, err := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "nik", request.NIK)
	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not exist.")
	}

	userEmail, _ := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "email", request.Email)
	if userEmail.NIK != "" && userEmail.NIK != request.NIK {
		return web.UserResponse{}, exception.ErrorBadRequest("Email exist.")
	}
	user.Email = request.Email

	if request.Phone != "" {
		userPhone, _ := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "phone", request.Phone)
		if userPhone.NIK != "" && userPhone.NIK != request.NIK {
			return web.UserResponse{}, exception.ErrorBadRequest("Phone exist.")
		}
		user.Phone = &request.Phone
	}

	if request.NomorWa != "" {
		userPhone, _ := service.UserRepository.FindUserDomainNotDeleteByQueryTx(ctx.Context(), "nomor_wa", request.NomorWa)
		if userPhone.NIK != "" && userPhone.NIK != request.NIK {
			return web.UserResponse{}, exception.ErrorBadRequest("Nomor WA exist.")
		}
		user.NomorWa = &request.NomorWa
	} else {
		user.NomorWa = &request.NomorWa
	}

	updated := time.Now()
	user.Name = request.Name
	user.UpdatedAt = &updated

	err = service.UserRepository.UpdateUserTx(ctx.Context(), domain.UserDomainTouser(user))
	if err != nil {
		return web.UserResponse{}, err
	}

	helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicProfile)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisSet(redisKey, domain.UserDomainTouser(user))
	if err != nil {
		log.Println(err)
	}

	return domain.ToUserResponseDomain(user), nil
}

func (service *UserServiceImpl) UpdatePasswordUser(ctx *fiber.Ctx, request web.UserUpdatePasswordRequest) (web.UserResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return web.UserResponse{}, err
	}

	if len(request.Password) < 6 {
		return web.UserResponse{}, exception.ErrorBadRequest("Password lenght should more then equal 6 character.")
	}

	user, err := service.UserRepository.FindUserNotDeleteByQueryTx(ctx.Context(), "nik", request.NIK)
	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not exist.")
	}

	user.SetPassword(request.Password)
	user.UpdatedAt = time.Now()

	err = service.UserRepository.UpdatePasswordTx(ctx.Context(), user)
	if err != nil {
		return web.UserResponse{}, err
	}

	helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicProfile)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisSet(redisKey, user)
	if err != nil {
		log.Println(err)
	}

	return domain.ToUserResponse(user), nil
}

func (service *UserServiceImpl) DeleteUser(ctx *fiber.Ctx, nik string) error {

	cookie := ctx.Cookies("token")
	actor, _, _, _ := helper.ParseJwt(cookie)

	if actor == nik {
		return exception.ErrorNotFound("You cant delete your own account.")
	}
	user, err := service.UserRepository.FindUserNotDeleteByQueryTx(ctx.Context(), "nik", nik)
	if err != nil || user.NIK == "" {
		return exception.ErrorNotFound("User not exist.")
	}

	deleted := time.Now()
	user.DeletedAt = &deleted
	user.UpdatedAt = time.Now()

	err = service.UserRepository.DeleteUserTx(ctx.Context(), user)
	if err != nil {
		return err
	}

	helper.ProduceToKafka(user, "DELETE.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "DELETE.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "DELETE.USER", config.KafkaTopicProfile)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisDel(redisKey)
	if err != nil {
		log.Println(err)
	}

	return nil
}
