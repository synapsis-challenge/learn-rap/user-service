package controller

import (
	"fmt"
	"strconv"
	"time"

	"learn-raa/user-service/config"
	"learn-raa/user-service/exception"
	"learn-raa/user-service/helper"
	"learn-raa/user-service/middleware"
	"learn-raa/user-service/model/web"
	"learn-raa/user-service/service"

	"github.com/gofiber/fiber/v2"
)

type UserControllerImpl struct {
	UserService service.UserService
}

type UserController interface {
	NewUserRouter(app *fiber.App)
}

func NewUserController(userService service.UserService) UserController {
	return &UserControllerImpl{
		UserService: userService,
	}
}

func (controller *UserControllerImpl) NewUserRouter(app *fiber.App) {
	user := app.Group(config.EndpointPrefix)
	user.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	user.Use(middleware.IsAuthenticated)
	user.Post("/", controller.CreateUser)
	user.Get("/", controller.GetAllUsers)
	user.Get("/:nik", controller.GetUserById)
	user.Put("/:nik", controller.UpdateUser)
	user.Put("/:nik/password", controller.UpdatePasswordUser)
	user.Delete("/:nik", controller.DeleteUser)
}

func (controller *UserControllerImpl) CreateUser(ctx *fiber.Ctx) error {
	var request web.UserCreateRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		exception.ErrorHandler(ctx, err)
	}

	active := 1
	request.Status = &active

	response, err := controller.UserService.CreateUser(ctx, request)

	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	cookieData := ctx.Cookies("token")
	actor, _, _, _ := helper.ParseJwt(cookieData)
	actors, _ := controller.UserService.FindUserNotDeleteByNik(ctx, actor)

	action := fmt.Sprintf("create user %s", response.NIK)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: actors.Name,
		Category:  config.CategoryService,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) GetUserById(ctx *fiber.Ctx) error {
	nik := ctx.Params("nik")

	response, err := controller.UserService.FindUserNotDeleteByNik(ctx, nik)

	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) GetAllUsers(ctx *fiber.Ctx) error {
	search := ctx.Query("search")
	departement := ctx.Query("departement")
	project := ctx.Query("project")
	position := ctx.Query("position")
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	searchName := ctx.Query("search-name")
	searchRole := ctx.Query("search-role")
	searchDepartement := ctx.Query("search-departement")
	searchPosition := ctx.Query("search-position")
	searchProject := ctx.Query("search-project")
	searchNik := ctx.Query("search-nik")

	response, err := controller.UserService.FindAllNotDeletedUser(ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	count, err := controller.UserService.CountAllNotDeletedUser(ctx, search, departement, project, position, page, limit, searchName, searchRole, searchDepartement, searchPosition, searchProject, searchNik)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	if page != "" {
		pageInt, _ := strconv.Atoi(page)
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponseWithPage{
			Code:      fiber.StatusOK,
			Status:    true,
			Page:      pageInt,
			Count:     len(response),
			TotalData: count,
			Message:   "success",
			Data:      response,
		})
	} else {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "success",
			Data:    response,
		})
	}
}

func (controller *UserControllerImpl) UpdateUser(ctx *fiber.Ctx) error {
	var request web.UserUpdateRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	nik := ctx.Params("nik")

	request.NIK = nik
	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	userResponse, err := controller.UserService.UpdateUser(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}
	actors, _ := controller.UserService.FindUserNotDeleteByNik(ctx, actor)
	action := fmt.Sprintf("updated user %s", userResponse.Name)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: actors.Name,
		Category:  config.CategoryService,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) UpdatePasswordUser(ctx *fiber.Ctx) error {
	var request web.UserUpdatePasswordRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	nik := ctx.Params("nik")

	request.NIK = nik
	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	userResponse, err := controller.UserService.UpdatePasswordUser(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	actors, _ := controller.UserService.FindUserNotDeleteByNik(ctx, actor)
	action := fmt.Sprintf("updated password user %s", userResponse.Name)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: actors.Name,
		Category:  config.CategoryService,
		// Project:   actors.ProjectName,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) DeleteUser(ctx *fiber.Ctx) error {
	nik := ctx.Params("nik")

	err := controller.UserService.DeleteUser(ctx, nik)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	actors, _ := controller.UserService.FindUserNotDeleteByNik(ctx, actor)
	action := fmt.Sprintf("delete user %s", nik)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: actors.Name,
		Category:  config.CategoryService,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
